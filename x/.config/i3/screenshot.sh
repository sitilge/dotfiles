#!/bin/sh

DIR=${HOME}/Screenshots

mkdir -p "${DIR}"

FILENAME="screenshot-$(date +%F-%T).png"
scrot -s -z "${DIR}"/"${FILENAME}"

#Create a link, so don't have to search for the newest
ln -sf "${DIR}"/"${FILENAME}" "${DIR}"/screenshot-latest.png

#Copy to the buffer
xclip -selection clipboard -t image/png "${DIR}"/screenshot-latest.png

#Send a notification
notify-send "Screenshot taken! See it here: ${DIR}/screenshot-latest.png"
