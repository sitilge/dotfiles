# Dotfiles

First, install `stow`, and then run the following command to set up a pack of dotfiles:

```sh
stow --no-folding -t ~ alacritty curl fonts git gpg htop mako ranger rofi ssh sway wofi x xdg zsh
sudo stow --no-folding -t / earlyoom pacman
```

## Notes

For more configuration, see the `unmaintained` branch.