#!/bin/sh

DIR=${HOME}/Screenshots

mkdir -p "${DIR}"

FILENAME="screenshot-$(date +%F-%T).png"
grim -g "$(slurp)" "${DIR}"/"${FILENAME}" || exit 1

#Create a link, so don't have to search for the newest
ln -sf "${DIR}"/"${FILENAME}" "${DIR}"/screenshot-latest.png

#Copy to the buffer
wl-copy < "${DIR}"/screenshot-latest.png

#Send a notification
notify-send "Screenshot taken! See it here: ${DIR}/screenshot-latest.png"
