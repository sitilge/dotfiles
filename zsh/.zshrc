# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$HOME/go/bin:$HOME/.local/bin:$PATH

# Path to your oh-my-zsh installation.
ZSH=/usr/share/oh-my-zsh/

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
# ZSH_THEME="random"

# I load a custom theme.
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
	asdf
	aws
	colored-man-pages
	direnv
	dotenv
	docker
	extract
	git
	golang
	helm
	kubectl
	systemd
	terraform
	vi-mode
	z
)

# User configuration
export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Personal preferences
export EDITOR=nvim
export VISUAL=nvim
export BROWSER=firefox

# Set env variable for GPG
export GPG_TTY=$TTY

# Solarized colors for tty
if [ "$TERM" = "linux" ]; then
	echo -en "\e]PB657b83" # S_base00
	echo -en "\e]PA586e75" # S_base01
	echo -en "\e]P0073642" # S_base02
	echo -en "\e]P62aa198" # S_cyan
	echo -en "\e]P8002b36" # S_base03
	echo -en "\e]P2859900" # S_green
	echo -en "\e]P5d33682" # S_magenta
	echo -en "\e]P1dc322f" # S_red
	echo -en "\e]PC839496" # S_base0
	echo -en "\e]PE93a1a1" # S_base1
	echo -en "\e]P9cb4b16" # S_orange
	echo -en "\e]P7eee8d5" # S_base2
	echo -en "\e]P4268bd2" # S_blue
	echo -en "\e]P3b58900" # S_yellow
	echo -en "\e]PFfdf6e3" # S_base3
	echo -en "\e]PD6c71c4" # S_violet
	clear # against bg artifacts
fi

ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then
	mkdir $ZSH_CACHE_DIR
fi

source $ZSH/oh-my-zsh.sh

# Aliases (have to be after sourcing the oh-my-zsh
# because it will rewrite my aliases otherwise)
alias bc='insect'
alias cal='cal -m3'
alias cat='bat'
alias cp='cp -vip'
alias df='duf --width 1000'
alias dmesg='dmesg -T --level=warn+' 
alias free='free -htw'
alias ip='ip -color=auto'
alias kctx='kubectx'
alias kdo='kubectl --dry-run=client -oyaml'
alias kns='kubens'
alias kubectl='kubecolor'
alias ll='ls -lhHiag'
alias l='ls -lhHig'
alias ls='exa --time-style=long-iso'
alias lt='ls -lhHigT'
alias mkdir='mkdir -vp'
alias mv='mv -vi'
alias rm='rm -vI'
alias ssh='TERM=xterm-256color \ssh'
alias top='bashtop'
alias vim='nvim'
alias xclip='xclip -selection clipboard'
alias ynv='yq -o json | jnv'

#Make completion work with kubecolor
compdef kubecolor=kubectl

#Reload the autocompletion
autoload -U compinit && compinit

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

#Source some zsh plugins
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
