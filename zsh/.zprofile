if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
    #Set the keyboard layout to Latvian
	export XKB_DEFAULT_LAYOUT=lv
	#This is needed for Goland
	export _JAVA_AWT_WM_NONREPARENTING=1
    #Start the ssh key agent (keychain)
	eval $(/usr/bin/keychain --eval --quiet)

	selection()
	{
		clear

		echo "Select the DE by entering the respective number!"
		echo "1) sway"
		echo "2) i3"
		echo "3) none"

		read de

		echo $de

		case $de in
			1|1\))
				clear
				systemctl --user set-environment WLR_NO_HARDWARE_CURSORS=1
				systemctl --user set-environment XDG_CURRENT_DESKTOP=sway
				systemctl --user set-environment XDG_SESSION_TYPE=wayland
				exec sway
				;;
			2|2\2)
				clear
				exec startx
				;;
			3|3\3)
				clear
				;;
			*)
				clear
				selection
				;;
		esac
	}

	selection
fi
